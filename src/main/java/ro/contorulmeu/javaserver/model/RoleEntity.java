package ro.contorulmeu.javaserver.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@Table(name = "role")
public class RoleEntity implements GrantedAuthority {

  @Id
  private String role;

  @Override
  public String getAuthority() {
    return role;
  }
}
