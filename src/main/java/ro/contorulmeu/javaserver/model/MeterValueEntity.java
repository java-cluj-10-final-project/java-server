package ro.contorulmeu.javaserver.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Getter
@Setter
@Entity
@Table(name = "meter_values")
public class MeterValueEntity {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(name = "updated")
  private LocalDateTime updateTime;

  @Column(name = "meter_value")
  private Double meterValue;

  @OneToOne
  @JoinColumn(name = "account_email")
  private AccountEntity account;

  @ManyToOne
  @JoinColumn(name = "meter_id")
  private MeterEntity meter;

}
