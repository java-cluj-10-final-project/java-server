package ro.contorulmeu.javaserver.repository;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ro.contorulmeu.javaserver.model.MeterEntity;

import java.util.List;

@Repository("meterRepo")
public interface MeterRepo extends JpaRepository<MeterEntity, Long> {

  @Query("SELECT m FROM MeterEntity m WHERE m.account.email=:email")
  List<MeterEntity> findAllByAccount(@Param("email") String email, Pageable pageable);

}
