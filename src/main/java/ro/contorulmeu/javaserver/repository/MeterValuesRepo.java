package ro.contorulmeu.javaserver.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ro.contorulmeu.javaserver.model.MeterValueEntity;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Repository("meterValuesRepo")
public interface MeterValuesRepo extends JpaRepository<MeterValueEntity, Long> {

  @Modifying
  @Query(value = "INSERT INTO meter_values (account_email, meter_id, updated, meter_value) VALUES (:accountEmail, :meterId, :updateTime, :value)", nativeQuery = true)
  int refresh(
      @Param("meterId") Long meterId,
      @Param("accountEmail") String accountEmail,
      @Param("updateTime") LocalDateTime updateTime,
      @Param("value") Double value
  );

  @Query("SELECT mv FROM MeterValueEntity mv WHERE mv.meter.id=:meterId")
  List<MeterValueEntity> findAllByMeter(@Param("meterId") Long meterId);

  @Query(value = "SELECT * FROM meter_values WHERE meter_id = :meterId ORDER BY updated DESC LIMIT 1", nativeQuery = true)
  Optional<MeterValueEntity> findLastByMeter(@Param("meterId") Long id);

}
