package ro.contorulmeu.javaserver.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ro.contorulmeu.javaserver.model.MeterInfoEntity;

import java.util.List;
import java.util.Optional;

@Repository("meterInfoRepo")
public interface MeterInfoRepo extends JpaRepository<MeterInfoEntity, Long> {

  @Modifying
  @Query(value = "INSERT INTO meter_info (meter_id, info_key, value) VALUES (:meterId, :meterInfoKey, :meterInfoValue)", nativeQuery = true)
  int addMeterInfo(
      @Param("meterId") Long meterId,
      @Param("meterInfoKey") String meterInfoKey,
      @Param("meterInfoValue") String meterInfoValue);

  @Modifying(clearAutomatically = true)
  @Query(value = "UPDATE meter_info SET value = :meterInfoValue WHERE id = :id", nativeQuery = true)
  void updateMeterInfo(
      @Param("id") Long id,
      @Param("meterInfoValue") String meterInfoValue);

  void deleteByIdAndKey(Long id, String key);

  Optional<MeterInfoEntity> findFirstByMeterIdAndKey(Long meterId, String key);

  void deleteByMeterIdAndKey(Long meterId, String key);

  @Query("SELECT mi FROM MeterInfoEntity mi WHERE mi.meter.id=:meterId")
  List<MeterInfoEntity> findAllByMeter(@Param("meterId") Long meterId);

}
