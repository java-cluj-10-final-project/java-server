package ro.contorulmeu.javaserver.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ro.contorulmeu.javaserver.model.AccountEntity;

@Repository("accountRepo")
public interface AccountRepo extends JpaRepository<AccountEntity, String> {
}
