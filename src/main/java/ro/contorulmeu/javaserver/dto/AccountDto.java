package ro.contorulmeu.javaserver.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

@Getter
@Setter
public class AccountDto extends AccountShortDto implements Serializable {

  private Set<RoleDto> roles;

  private List<MeterShortDto> meters;

}
