package ro.contorulmeu.javaserver.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class AccountAuthDto extends AccountShortDto implements Serializable {

  protected String password;

}
