package ro.contorulmeu.javaserver.dto;

import com.sun.istack.NotNull;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.time.LocalDateTime;

@Getter
@Setter
public class MeterValueDto implements Serializable {

  private Long id;

  private LocalDateTime updateTime;

  @NotNull
  private Double meterValue;

  private AccountShortDto account;

}
