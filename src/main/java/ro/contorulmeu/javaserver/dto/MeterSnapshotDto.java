package ro.contorulmeu.javaserver.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class MeterSnapshotDto extends MeterShortDto implements Serializable {

  private MeterValueDto value;
  private AccountShortDto account;
}
