package ro.contorulmeu.javaserver.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.sun.istack.NotNull;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.time.LocalDateTime;

@Getter
@Setter
public class AccountShortDto implements Serializable {

  @NotNull
  @JsonProperty("username")
  private String email;

  private LocalDateTime createTime;

  private LocalDateTime updateTime;

}
