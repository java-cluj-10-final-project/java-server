package ro.contorulmeu.javaserver.dto;

import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Getter
@Setter
public class ErrorResponse {
  private int httpStatusCode;
  private HttpStatus httpStatus;
  private String reason;
  private String message;
  private String timestamp;

  public ErrorResponse(HttpStatus httpStatus, String message) {
    this.httpStatusCode = httpStatus.value();
    this.httpStatus = httpStatus;
    this.reason = httpStatus.getReasonPhrase();
    this.message = message;
    this.timestamp = LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd-MMM-yyyy hh:mm:ss"));
  }
}
