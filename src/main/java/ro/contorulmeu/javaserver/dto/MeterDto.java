package ro.contorulmeu.javaserver.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
public class MeterDto extends MeterShortDto implements Serializable {

  private List<MeterValueDto> values;

  private List<MeterInfoDto> infos;

}
