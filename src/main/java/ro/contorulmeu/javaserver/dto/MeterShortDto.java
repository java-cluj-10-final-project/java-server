package ro.contorulmeu.javaserver.dto;

import com.sun.istack.NotNull;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class MeterShortDto implements Serializable {

  private Long id;
  @NotNull
  private String name;
  private String description;
  @NotNull
  private String iso;
}
