package ro.contorulmeu.javaserver.dto;

import com.sun.istack.NotNull;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class MeterInfoDto implements Serializable {

  private Long id;

  @NotNull
  private String key;

  @NotNull
  private String value;

}
