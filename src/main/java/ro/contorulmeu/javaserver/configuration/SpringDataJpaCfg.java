package ro.contorulmeu.javaserver.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EnableJpaRepositories(basePackages = {"ro/contorulmeu/javaserver/repository"})
public class SpringDataJpaCfg {

}
