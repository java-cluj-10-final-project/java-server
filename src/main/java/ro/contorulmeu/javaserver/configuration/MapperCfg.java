package ro.contorulmeu.javaserver.configuration;

import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MapperCfg {

  @Bean(name = "modelMapper")
  public ModelMapper getMapper() {
    return new ModelMapper();
  }

}
