package ro.contorulmeu.javaserver.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import ro.contorulmeu.javaserver.constants.SecurityConstants;
import ro.contorulmeu.javaserver.security.filter.JwtAccessDeniedHandler;
import ro.contorulmeu.javaserver.security.filter.JwtAuthenticationEntryPoint;
import ro.contorulmeu.javaserver.security.filter.JwtAuthorizationFilter;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityCfg extends WebSecurityConfigurerAdapter {

  private final PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
  @Autowired
  @Qualifier("jwtAuthorizationFilter")
  private JwtAuthorizationFilter jwtAuthorizationFilter;
  @Autowired
  @Qualifier("jwtAuthenticationEntryPoint")
  private JwtAuthenticationEntryPoint jwtAuthenticationEntryPoint;
  @Autowired
  @Qualifier("jwtAccessDeniedHandler")
  private JwtAccessDeniedHandler jwtAccessDeniedHandler;
  @Autowired
  @Qualifier("userDetailsService")
  private UserDetailsService userDetailService;

  @Override
  protected void configure(AuthenticationManagerBuilder auth) throws Exception {
    auth
        .userDetailsService(userDetailService)
        .passwordEncoder(passwordEncoder);
  }

  @Override
  protected void configure(HttpSecurity http) throws Exception {

    http
        .csrf().disable()
        .cors().and()

        .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS) // we do not save any info on the session because we use the jwt for that

        .and().authorizeRequests().antMatchers(SecurityConstants.PUBLIC_URLS).permitAll() // allow those urls to be accessible without authentication
        .anyRequest().authenticated() // any other endpoints are restricted to logged users

        .and().exceptionHandling()
        .accessDeniedHandler(jwtAccessDeniedHandler) // what to send when the access is denied
        .authenticationEntryPoint(jwtAuthenticationEntryPoint) // what to send when the authentication failed

        .and().addFilterBefore(jwtAuthorizationFilter, UsernamePasswordAuthenticationFilter.class);

    // .httpBasic(); // what to use for authorization
  }

  @Bean
  @Override
  public AuthenticationManager authenticationManager() throws Exception {
    return super.authenticationManager();
  }

}
