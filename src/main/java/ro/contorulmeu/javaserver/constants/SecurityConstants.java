package ro.contorulmeu.javaserver.constants;

public class SecurityConstants {
  public static final int EXPIRATION_TIME = 14_400;
  public static final String TOKEN_PREFIX = "Bearer ";
  public static final String JWT_TOKEN_HEADER = "Jwt-Token";
  public static final String TOKEN_CANNOT_BE_VERIFIED = "Token cannot be verified";
  public static final String ISSUER = "MY METER";
  public static final String GET_ARRAYS_ADMINISTRATION = "User Management Portal";
  public static final String AUTHORITIES = "authorities";
  public static final String FORBIDDEN_MESSAGE = "You need to log in to access this endpoint";
  public static final String ACCESS_DENIED_MESSAGE = "You do not have permission to access this endpoint";
  public static final String OPTIONS_HTTP_METHOD = "OPTIONS";
  public static final String[] PUBLIC_URLS = {"/user/login", "/user/register", "/user/resetpassword/**"};
}
