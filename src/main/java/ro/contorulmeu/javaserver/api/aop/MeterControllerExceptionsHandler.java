package ro.contorulmeu.javaserver.api.aop;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import ro.contorulmeu.javaserver.api.controller.MeterController;
import ro.contorulmeu.javaserver.dto.ErrorResponse;
import ro.contorulmeu.javaserver.exception.NotFoundException;
import ro.contorulmeu.javaserver.exception.OperationFailedException;
import ro.contorulmeu.javaserver.exception.ParameterConstraintViolationException;

import javax.annotation.Priority;


@ControllerAdvice(assignableTypes = {MeterController.class})
@Priority(2)
public class MeterControllerExceptionsHandler {
  private static final Logger LOG = LoggerFactory.getLogger(MeterControllerExceptionsHandler.class);

  @ExceptionHandler(NotFoundException.class)
  @ResponseStatus(code = HttpStatus.NOT_FOUND)
  @ResponseBody
  public ErrorResponse handleNotFoundException(NotFoundException exception) {
    LOG.error("NotFoundException", exception);
    return new ErrorResponse(HttpStatus.NOT_FOUND, exception.getMessage());
  }

  @ExceptionHandler({OperationFailedException.class, ParameterConstraintViolationException.class})
  @ResponseStatus(code = HttpStatus.BAD_REQUEST)
  @ResponseBody
  public ErrorResponse handleNotFoundException(RuntimeException exception) {
    LOG.error("OperationFailed", exception);
    return new ErrorResponse(HttpStatus.BAD_REQUEST, exception.getMessage());
  }

}
