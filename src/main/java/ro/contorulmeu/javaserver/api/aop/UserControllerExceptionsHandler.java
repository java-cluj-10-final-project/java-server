package ro.contorulmeu.javaserver.api.aop;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import ro.contorulmeu.javaserver.api.controller.UserController;
import ro.contorulmeu.javaserver.dto.ErrorResponse;
import ro.contorulmeu.javaserver.exception.WrongCredentialsException;

import javax.annotation.Priority;

@ControllerAdvice(assignableTypes = {UserController.class})
@Priority(2)
public class UserControllerExceptionsHandler {
    private final static Logger LOG = LoggerFactory.getLogger(UserControllerExceptionsHandler.class);

    @ExceptionHandler(WrongCredentialsException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    @ResponseBody
    public ErrorResponse handleWrongCredentialsException(WrongCredentialsException exception) {
        LOG.error("NotFoundException", exception);
        ErrorResponse result = new ErrorResponse(HttpStatus.UNAUTHORIZED, exception.getMessage());
        return result;
    }
}
