package ro.contorulmeu.javaserver.api.aop;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import ro.contorulmeu.javaserver.dto.ErrorResponse;

import javax.annotation.Priority;

@ControllerAdvice
@Priority(9)
public class GenericExceptionHandler {
  private final static Logger LOG = LoggerFactory.getLogger(GenericExceptionHandler.class);

  @ExceptionHandler(RuntimeException.class)
  @ResponseStatus(code = HttpStatus.INTERNAL_SERVER_ERROR)
  @ResponseBody
  public ErrorResponse handleNotFoundException(RuntimeException exception) {
    LOG.error("RuntimeException", exception);
    return new ErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR, "The operation failed.");
  }

  @ExceptionHandler(DataIntegrityViolationException.class)
  @ResponseStatus(code = HttpStatus.BAD_REQUEST)
  @ResponseBody
  public ErrorResponse handleNotFoundException(DataIntegrityViolationException exception) {
    LOG.error("RuntimeException", exception);
    return new ErrorResponse(HttpStatus.BAD_REQUEST, "Data integrity was violated.");
  }
}
