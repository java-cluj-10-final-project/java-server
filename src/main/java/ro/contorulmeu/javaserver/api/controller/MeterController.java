package ro.contorulmeu.javaserver.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import ro.contorulmeu.javaserver.dto.MeterDto;
import ro.contorulmeu.javaserver.dto.MeterInfoDto;
import ro.contorulmeu.javaserver.dto.MeterShortDto;
import ro.contorulmeu.javaserver.dto.MeterSnapshotDto;
import ro.contorulmeu.javaserver.dto.MeterValueDto;
import ro.contorulmeu.javaserver.service.MeterService;

import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping("/api/v1/meter")
@CrossOrigin(origins = {"*"}, methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT, RequestMethod.PATCH, RequestMethod.DELETE})
public class MeterController {

    private final MeterService meterService;

    @Autowired
    public MeterController(@Qualifier("defaultMeterService") MeterService meterService) {
        this.meterService = meterService;
    }

    @GetMapping("/{id}")
    public MeterDto getMeterById(@PathVariable("id") Long id) {

        return meterService.getById(id);
    }

    @GetMapping
    public List<MeterSnapshotDto> getAllMeters(
        @RequestParam(name = "page", defaultValue = "0") int page,
        @RequestParam(name = "size", defaultValue = "10") int size) {

        return meterService.getAll(page, size);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public MeterShortDto createMeter(
        @RequestBody MeterShortDto meter
    ) {

        return meterService.create(meter);
    }

    @PatchMapping("/{id}")
    @ResponseStatus(HttpStatus.CREATED)
    public MeterShortDto updateMeter(
        @PathVariable("id") Long id, @RequestBody MeterShortDto meter) {

        return meterService.update(id, meter);
    }

    @PostMapping("/{id}/value")
    @ResponseStatus(HttpStatus.CREATED)
    public List<MeterValueDto> refreshMeter(
        @PathVariable("id") Long id, @RequestBody MeterValueDto value) {

        if (value.getUpdateTime() == null) {
            value.setUpdateTime(LocalDateTime.now());
        }

        return meterService.refresh(id, value);
    }

    @PostMapping("/{meter-id}/info")
    @ResponseStatus(HttpStatus.CREATED)
    public List<MeterInfoDto> setMeterInfo(
        @PathVariable("meter-id") Long id, @RequestBody MeterInfoDto info) {

        return meterService.setInfo(id, info);
    }

    @PatchMapping("/{info-id}/info")
    @ResponseStatus(HttpStatus.CREATED)
    public List<MeterInfoDto> editMeterInfo(
        @PathVariable("info-id") Long id, @RequestBody MeterInfoDto info) {

        return meterService.editMeterInfo(id, info);
    }

    @DeleteMapping("/{info-id}/info")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void removeMeterInfo(
        @PathVariable("info-id") long id) {

        meterService.removeInfo(id);
    }

    @DeleteMapping("/{value-id}/value")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void removeMeterValue(
        @PathVariable("value-id") Long id) {

        meterService.removeValue(id);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteMeter(
        @PathVariable("id") Long id) {

        meterService.delete(id);
    }

}
