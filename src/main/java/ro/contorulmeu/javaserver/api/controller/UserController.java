package ro.contorulmeu.javaserver.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ro.contorulmeu.javaserver.dto.AccountAuthDto;
import ro.contorulmeu.javaserver.dto.TokenDto;
import ro.contorulmeu.javaserver.facade.AccountFacade;

@RestController
@RequestMapping("/user")
@CrossOrigin(origins = {"*"}, methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT, RequestMethod.PATCH, RequestMethod.DELETE})
public class UserController {

    private final AccountFacade accountFacade;

    @Autowired
    public UserController(@Qualifier("accountFacade") AccountFacade accountFacade) {
        this.accountFacade = accountFacade;
    }

    @PostMapping("/register")
    public ResponseEntity<Void> register(@RequestBody AccountAuthDto user) {
        this.accountFacade.register(user);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @PostMapping("/login")
    public ResponseEntity<Object> login(@RequestBody AccountAuthDto account) {
        String token = accountFacade.login(account);
        return new ResponseEntity<>(new TokenDto(token), HttpStatus.OK);
    }
}
