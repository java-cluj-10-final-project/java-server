package ro.contorulmeu.javaserver.mapper;

import ro.contorulmeu.javaserver.dto.AccountAuthDto;
import ro.contorulmeu.javaserver.dto.AccountDto;
import ro.contorulmeu.javaserver.dto.AccountShortDto;
import ro.contorulmeu.javaserver.dto.MeterDto;
import ro.contorulmeu.javaserver.dto.MeterInfoDto;
import ro.contorulmeu.javaserver.dto.MeterShortDto;
import ro.contorulmeu.javaserver.dto.MeterSnapshotDto;
import ro.contorulmeu.javaserver.dto.MeterValueDto;
import ro.contorulmeu.javaserver.model.AccountEntity;
import ro.contorulmeu.javaserver.model.MeterEntity;
import ro.contorulmeu.javaserver.model.MeterInfoEntity;
import ro.contorulmeu.javaserver.model.MeterValueEntity;

import java.util.List;

public interface Mapper {

  AccountDto toDto(AccountEntity accountEntity, List<MeterEntity> meterEntities);

  AccountShortDto toDto(AccountEntity accountEntity);

  AccountAuthDto toDto(String email, String password);

  MeterDto toDto(MeterEntity meterEntity, List<MeterValueEntity> meterValueEntities, List<MeterInfoEntity> meterInfoEntities);

  MeterSnapshotDto toDto(MeterEntity meterEntity);

  MeterValueDto toDto(MeterValueEntity meterValueEntity);

  MeterInfoDto toDto(MeterInfoEntity meterInfoEntity);


  AccountEntity toEntity(AccountDto accountDto);

  AccountEntity toEntity(AccountShortDto accountDto);

  AccountEntity toEntity(AccountAuthDto accountDto);

  MeterEntity toEntity(MeterDto meterDto);

  MeterEntity toEntity(MeterShortDto meterDto);

  MeterValueEntity toEntity(MeterValueDto meterValueDto);

  MeterInfoEntity toEntity(MeterInfoDto meterInfoDto);

}
