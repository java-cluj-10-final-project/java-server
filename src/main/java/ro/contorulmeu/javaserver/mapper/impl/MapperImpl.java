package ro.contorulmeu.javaserver.mapper.impl;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import ro.contorulmeu.javaserver.dto.AccountAuthDto;
import ro.contorulmeu.javaserver.dto.AccountDto;
import ro.contorulmeu.javaserver.dto.AccountShortDto;
import ro.contorulmeu.javaserver.dto.MeterDto;
import ro.contorulmeu.javaserver.dto.MeterInfoDto;
import ro.contorulmeu.javaserver.dto.MeterShortDto;
import ro.contorulmeu.javaserver.dto.MeterSnapshotDto;
import ro.contorulmeu.javaserver.dto.MeterValueDto;
import ro.contorulmeu.javaserver.mapper.Mapper;
import ro.contorulmeu.javaserver.model.AccountEntity;
import ro.contorulmeu.javaserver.model.MeterEntity;
import ro.contorulmeu.javaserver.model.MeterInfoEntity;
import ro.contorulmeu.javaserver.model.MeterValueEntity;
import ro.contorulmeu.javaserver.repository.MeterValuesRepo;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component("mapper")
public class MapperImpl implements Mapper {

    private final ModelMapper mapper;
    private final MeterValuesRepo meterValuesRepo;

    @Autowired
    public MapperImpl(
        @Qualifier("modelMapper") ModelMapper mapper,
        @Qualifier("meterValuesRepo") MeterValuesRepo meterValuesRepo) {
        this.mapper = mapper;
        this.meterValuesRepo = meterValuesRepo;
    }

    @Override
    public AccountDto toDto(AccountEntity accountEntity, List<MeterEntity> meterEntities) {
        AccountDto result = mapper.map(accountEntity, AccountDto.class);

        List<MeterShortDto> meters = meterEntities
            .parallelStream()
            .map(this::toDto)
            .collect(Collectors.toList());
        result.setMeters(meters);

        return result;
    }

    @Override
    public AccountShortDto toDto(AccountEntity accountEntity) {
        return mapper.map(accountEntity, AccountShortDto.class);
    }

    @Override
    public AccountAuthDto toDto(String email, String password) {
        AccountAuthDto accountAuthDto = new AccountAuthDto();
        accountAuthDto.setEmail(email);
        accountAuthDto.setPassword(password);
        return accountAuthDto;
    }

    @Override
    public MeterDto toDto(MeterEntity meterEntity, List<MeterValueEntity> meterValueEntities, List<MeterInfoEntity> meterInfoEntities) {
        MeterDto result = mapper.map(meterEntity, MeterDto.class);

        List<MeterValueDto> meterValues = meterValueEntities
            .stream()
            .map(this::toDto)
            .collect(Collectors.toList());
        result.setValues(meterValues);

        List<MeterInfoDto> meterInfos = meterInfoEntities
            .parallelStream()
            .map(this::toDto)
            .collect(Collectors.toList());
        result.setInfos(meterInfos);

        return result;
    }

    @Override
    public MeterSnapshotDto toDto(MeterEntity meterEntity) {
        MeterSnapshotDto result = mapper.map(meterEntity, MeterSnapshotDto.class);

        Optional<MeterValueEntity> lastValueOptional = meterValuesRepo.findLastByMeter(meterEntity.getId());
        MeterValueDto lastMeterUpdateDto = lastValueOptional
            .map(meterValueEntity -> mapper.map(meterValueEntity, MeterValueDto.class))
            .orElse(null);

        result.setValue(lastMeterUpdateDto);
        return result;
    }

    @Override
    public MeterValueDto toDto(MeterValueEntity meterValueEntity) {
        return mapper.map(meterValueEntity, MeterValueDto.class);
    }

    @Override
    public MeterInfoDto toDto(MeterInfoEntity meterInfoEntity) {
        return mapper.map(meterInfoEntity, MeterInfoDto.class);
    }

    @Override
    public AccountEntity toEntity(AccountDto accountDto) {
        return mapper.map(accountDto, AccountEntity.class);
    }

    @Override
    public AccountEntity toEntity(AccountShortDto accountDto) {
        return mapper.map(accountDto, AccountEntity.class);
    }

    @Override
    public AccountEntity toEntity(AccountAuthDto accountDto) {
        return mapper.map(accountDto, AccountEntity.class);
    }

    @Override
    public MeterEntity toEntity(MeterDto meterDto) {
        return mapper.map(meterDto, MeterEntity.class);
    }

    @Override
    public MeterEntity toEntity(MeterShortDto meterDto) {
        return mapper.map(meterDto, MeterEntity.class);
    }

    @Override
    public MeterValueEntity toEntity(MeterValueDto meterValueDto) {
        return mapper.map(meterValueDto, MeterValueEntity.class);
    }

    @Override
    public MeterInfoEntity toEntity(MeterInfoDto meterInfoDto) {
        return mapper.map(meterInfoDto, MeterInfoEntity.class);
    }
}
