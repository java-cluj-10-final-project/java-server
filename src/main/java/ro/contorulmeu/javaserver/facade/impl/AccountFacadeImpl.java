package ro.contorulmeu.javaserver.facade.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import ro.contorulmeu.javaserver.dto.AccountAuthDto;
import ro.contorulmeu.javaserver.exception.WrongCredentialsException;
import ro.contorulmeu.javaserver.facade.AccountFacade;
import ro.contorulmeu.javaserver.service.AccountService;

@Service("accountFacade")
public class AccountFacadeImpl implements AccountFacade {

    private final AccountService service;

    @Value("${auth.pass.special_characters}")
    private String passSpecialCharacters;

    @Autowired
    public AccountFacadeImpl(@Qualifier("userDetailsService") AccountService service) {
        this.service = service;
    }

    @Override
    public void register(AccountAuthDto user) {
        if (!service.validateNewEmail(user.getEmail())) {
            throw new WrongCredentialsException(String.format("%s email cannot be used", user.getEmail()));
        }
        if (!service.validateNewPassword(user.getPassword())) {
            throw new WrongCredentialsException(String.format("The password must contain at least one uppercase letter, one lowercase letter, a number, and a special character (%s) and be between 8 and 16 characters long", passSpecialCharacters));
        }
        service.registerUser(user);
    }

    @Override
    public String login(AccountAuthDto account) {
        if (service.validateCredentials(account)) {
            return service.generateJwt(account);
        } else {
            throw new WrongCredentialsException("username or password is wrong");
        }
    }
}
