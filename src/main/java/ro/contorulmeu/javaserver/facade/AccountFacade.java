package ro.contorulmeu.javaserver.facade;

import ro.contorulmeu.javaserver.dto.AccountAuthDto;

public interface AccountFacade {

    void register(AccountAuthDto user);

    String login(AccountAuthDto account);

}
