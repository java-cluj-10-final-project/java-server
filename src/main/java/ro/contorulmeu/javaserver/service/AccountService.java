package ro.contorulmeu.javaserver.service;

import ro.contorulmeu.javaserver.dto.AccountAuthDto;
import ro.contorulmeu.javaserver.dto.AccountDto;
import ro.contorulmeu.javaserver.dto.AccountShortDto;

public interface AccountService {

  void registerUser(AccountAuthDto user);

  AccountDto getFullByEmail(final String email);

  AccountShortDto getShortByEmail(final String email);

  AccountAuthDto getAuthByEmail(final String email);

  String generateJwt(AccountAuthDto account);

  boolean validateCredentials(AccountAuthDto user);

  boolean validateNewEmail(String email);

  boolean validateNewPassword(String password);
}
