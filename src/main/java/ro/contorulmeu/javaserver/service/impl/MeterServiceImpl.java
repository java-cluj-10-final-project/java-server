package ro.contorulmeu.javaserver.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ro.contorulmeu.javaserver.dto.MeterDto;
import ro.contorulmeu.javaserver.dto.MeterInfoDto;
import ro.contorulmeu.javaserver.dto.MeterShortDto;
import ro.contorulmeu.javaserver.dto.MeterSnapshotDto;
import ro.contorulmeu.javaserver.dto.MeterValueDto;
import ro.contorulmeu.javaserver.exception.NotFoundException;
import ro.contorulmeu.javaserver.exception.OperationFailedException;
import ro.contorulmeu.javaserver.exception.ParameterConstraintViolationException;
import ro.contorulmeu.javaserver.mapper.Mapper;
import ro.contorulmeu.javaserver.model.AccountEntity;
import ro.contorulmeu.javaserver.model.MeterEntity;
import ro.contorulmeu.javaserver.model.MeterInfoEntity;
import ro.contorulmeu.javaserver.model.MeterValueEntity;
import ro.contorulmeu.javaserver.repository.AccountRepo;
import ro.contorulmeu.javaserver.repository.MeterInfoRepo;
import ro.contorulmeu.javaserver.repository.MeterRepo;
import ro.contorulmeu.javaserver.repository.MeterValuesRepo;
import ro.contorulmeu.javaserver.service.MeterService;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service("defaultMeterService")
@Transactional
public class MeterServiceImpl implements MeterService {
    private final String METER_NOT_FOUND = "Meter not found";
    private final String METER_INFO_NOT_FOUND = "Meter info not found";
    private final String METER_VALUE_NOT_FOUND = "Meter value not found";

    private final Mapper mapper;

    private final MeterRepo meterRepo;

    private final MeterValuesRepo meterValuesRepo;

    private final MeterInfoRepo meterInfoRepo;

    private final AccountRepo accountRepo;

    @Autowired
    public MeterServiceImpl(
        @Qualifier("mapper") Mapper mapper,
        @Qualifier("meterRepo") MeterRepo meterRepo,
        @Qualifier("meterValuesRepo") MeterValuesRepo meterValuesRepo,
        @Qualifier("meterInfoRepo") MeterInfoRepo meterInfoRepo,
        @Qualifier("accountRepo") AccountRepo accountRepo) {
        this.mapper = mapper;
        this.meterRepo = meterRepo;
        this.meterValuesRepo = meterValuesRepo;
        this.meterInfoRepo = meterInfoRepo;
        this.accountRepo = accountRepo;
    }

    @Override
    public MeterDto getById(final Long id) {

        String email = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        MeterEntity meterEntity = meterRepo
            .findById(id)
            .orElseThrow(() -> new NotFoundException(METER_NOT_FOUND));

        if (!meterEntity.getAccount().getEmail().equals(email)) {
            throw new NotFoundException(METER_NOT_FOUND);
        }

        List<MeterValueEntity> meterValues = meterValuesRepo.findAllByMeter(id);
        List<MeterInfoEntity> meterInfos = meterInfoRepo.findAllByMeter(id);

        return mapper.toDto(meterEntity, meterValues, meterInfos);
    }

    @Override
    public List<MeterSnapshotDto> getAll(final Integer page, final Integer size) {
        String email = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        Pageable pageable = PageRequest.of(page, size);
        List<MeterEntity> allByAccount = meterRepo.findAllByAccount(email, pageable);

        return allByAccount
            .parallelStream()
            .map(mapper::toDto)
            .collect(Collectors.toList());
    }

    @Override
    public MeterShortDto create(final MeterShortDto meter) {

        if ((meter.getName() != null && meter.getName().length() == 0) || (meter.getIso() != null && meter.getIso().length() == 0)) {
            throw new ParameterConstraintViolationException("Name and ISO parameters cannot be set to an empty string.");
        }

        String email = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        MeterEntity meterEntity = mapper.toEntity(meter);
        meterEntity.setId(null);

        AccountEntity account = accountRepo.findById(email).get();
        meterEntity.setAccount(account);

        MeterEntity result = meterRepo.saveAndFlush(meterEntity);

        return mapper.toDto(result);
    }

    @Override
    public MeterShortDto update(final Long meterId, final MeterShortDto meter) {

        if ((meter.getName() != null && meter.getName().length() == 0) || (meter.getIso() != null && meter.getIso().length() == 0)) {
            throw new ParameterConstraintViolationException("Name and ISO parameters cannot be set to an empty string.");
        }

        String email = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        Optional<MeterEntity> meterOptional = meterRepo.findById(meterId);

        if (meterOptional.isEmpty() || !meterOptional.get().getAccount().getEmail().equals(email)) {
            throw new NotFoundException(METER_NOT_FOUND);
        }

        MeterEntity current = meterOptional.get();

        current.setName((meter.getName() != null) ? meter.getName() : current.getName());
        current.setDescription((meter.getDescription() != null) ? meter.getDescription() : current.getDescription());
        current.setIso((meter.getIso() != null) ? meter.getIso() : current.getIso());

        MeterEntity updated = meterRepo.saveAndFlush(current);

        return mapper.toDto(updated);
    }

    @Override
    public void delete(final Long id) {

        String email = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        Optional<MeterEntity> byId = meterRepo.findById(id);

        if (byId.isEmpty() || !byId.get().getAccount().getEmail().equals(email)) {
            throw new NotFoundException(METER_NOT_FOUND);
        }

        try {
            meterRepo.deleteById(id);
        } catch (IllegalArgumentException e) {
            throw new OperationFailedException("The id cannot be null");
        }
    }

    @Override
    public List<MeterValueDto> refresh(final Long meterId, final MeterValueDto meterValue) {

        String email = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        Optional<MeterEntity> byId = meterRepo.findById(meterId);

        if (byId.isEmpty() || !byId.get().getAccount().getEmail().equals(email)) {
            throw new NotFoundException(METER_NOT_FOUND);
        }

        try {
            meterValuesRepo.refresh(
                meterId,
                (meterValue.getAccount() != null && meterValue.getAccount().getEmail() != null) ? meterValue.getAccount().getEmail() : email,
                meterValue.getUpdateTime(),
                meterValue.getMeterValue()
            );

        } catch (Exception e) {
            throw new OperationFailedException(String.format("Could not save the value of %f", meterValue.getMeterValue()));
        }

        return meterValuesRepo.findAllByMeter(meterId)
            .parallelStream()
            .map(mapper::toDto)
            .collect(Collectors.toList());
    }

    @Override
    public List<MeterInfoDto> setInfo(final Long meterId, final MeterInfoDto meterInfo) {

        if ((meterInfo.getKey() != null && meterInfo.getKey().length() == 0) || (meterInfo.getValue() != null && meterInfo.getValue().length() == 0)) {
            throw new ParameterConstraintViolationException("Key and Value parameters cannot be set to an empty string.");
        }

        String email = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        Optional<MeterEntity> byId = meterRepo.findById(meterId);

        if (byId.isEmpty() || !byId.get().getAccount().getEmail().equals(email)) {
            throw new NotFoundException(METER_NOT_FOUND);
        }

        Optional<MeterInfoEntity> existingOptional = meterInfoRepo.findFirstByMeterIdAndKey(meterId, meterInfo.getKey());

        try {
            if (existingOptional.isEmpty()) {
                meterInfoRepo.addMeterInfo(meterId, meterInfo.getKey(), meterInfo.getValue());
            } else {
                MeterInfoEntity infoExisting = existingOptional.get();
                meterInfoRepo.updateMeterInfo(infoExisting.getId(), meterInfo.getValue());
            }
        } catch (Exception e) {
            throw new OperationFailedException(
                String.format("Could not set requested info on the meter with id %d", meterId),
                e);
        }

        return meterInfoRepo.findAllByMeter(meterId)
            .parallelStream()
            .map(mapper::toDto)
            .collect(Collectors.toList());
    }

    @Override
    public List<MeterInfoDto> editMeterInfo(Long meterInfoId, MeterInfoDto meterInfo) {

        if ((meterInfo.getKey() == null || meterInfo.getKey().isBlank()) || (meterInfo.getValue() == null || meterInfo.getValue().isBlank())) {
            throw new ParameterConstraintViolationException("Key and Value parameters cannot be set to an empty string.");
        }

        Optional<MeterInfoEntity> infoOptional = meterInfoRepo.findById(meterInfoId);
        if (infoOptional.isEmpty()) {
            throw new NotFoundException(METER_NOT_FOUND);
        }

        String email = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        MeterInfoEntity info = infoOptional.get();
        Optional<MeterEntity> meterOptional = meterRepo.findById(info.getMeter().getId());
        if (meterOptional.isEmpty() || !meterOptional.get().getAccount().getEmail().equals(email)) {
            throw new NotFoundException(METER_NOT_FOUND);
        }

        info.setKey(meterInfo.getKey());
        info.setValue(meterInfo.getValue());

        try {
            meterInfoRepo.save(info);
        } catch (Exception e) {
            throw new OperationFailedException(String.format("Could not set requested info on the meter %s", meterOptional.get().getName()), e);
        }

        return meterInfoRepo.findAllByMeter(meterOptional.get().getId())
            .parallelStream()
            .map(mapper::toDto)
            .collect(Collectors.toList());
    }

    @Override
    public List<MeterInfoDto> removeInfo(final Long meterInfoId) {

        // check availability of the info
        Optional<MeterInfoEntity> meterInfoOptional = meterInfoRepo.findById(meterInfoId);

        if (meterInfoOptional.isEmpty()) {
            throw new NotFoundException(METER_INFO_NOT_FOUND);
        }

        // check access to the info
        Long meterId = meterInfoOptional.get().getMeter().getId();
        Optional<MeterEntity> meterOptional = meterRepo.findById(meterId);
        String email = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (meterOptional.isEmpty() || !meterOptional.get().getAccount().getEmail().equals(email)) {
            throw new NotFoundException(METER_NOT_FOUND);
        }

        // perform the deletion
        try {
            meterInfoRepo.deleteById(meterInfoId);
        } catch (Exception e) {
            throw new OperationFailedException("Could not delete the meter info");
        }

        return meterInfoRepo.findAllByMeter(meterId)
            .parallelStream()
            .map(mapper::toDto)
            .collect(Collectors.toList());
    }

    @Override
    public List<MeterValueDto> removeValue(final Long meterValueId) {

        // check availability of the value
        Optional<MeterValueEntity> meterValueOptional = meterValuesRepo.findById(meterValueId);

        if (meterValueOptional.isEmpty()) {
            throw new NotFoundException(METER_VALUE_NOT_FOUND);
        }

        // check access to the value
        Long meterId = meterValueOptional.get().getMeter().getId();
        Optional<MeterEntity> meterOptional = meterRepo.findById(meterId);
        String email = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (meterOptional.isEmpty() || !meterOptional.get().getAccount().getEmail().equals(email)) {
            throw new NotFoundException(METER_NOT_FOUND);
        }

        // perform the deletion
        try {
            meterValuesRepo.deleteById(meterValueId);
        } catch (Exception e) {
            throw new OperationFailedException("Could not delete the meter info");
        }

        return meterValuesRepo.findAllByMeter(meterId)
            .parallelStream()
            .map(mapper::toDto)
            .collect(Collectors.toList());
    }
}
