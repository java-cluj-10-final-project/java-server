package ro.contorulmeu.javaserver.service.impl;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ro.contorulmeu.javaserver.dto.AccountAuthDto;
import ro.contorulmeu.javaserver.dto.AccountDto;
import ro.contorulmeu.javaserver.dto.AccountShortDto;
import ro.contorulmeu.javaserver.exception.NotFoundException;
import ro.contorulmeu.javaserver.mapper.Mapper;
import ro.contorulmeu.javaserver.model.AccountEntity;
import ro.contorulmeu.javaserver.repository.AccountRepo;
import ro.contorulmeu.javaserver.security.utils.JwtTokenProvider;
import ro.contorulmeu.javaserver.service.AccountService;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.Optional;

@Service("userDetailsService")
@Transactional
public class UserDetailsServiceImpl implements UserDetailsService, AccountService {

    private static final PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

    @Value("${auth.pass.special_characters}")
    private String passSpecialCharacters;

    private final AccountRepo repo;

    private final Mapper mapper;

    private final JwtTokenProvider jwtTokenProvider;

    public UserDetailsServiceImpl(@Qualifier("accountRepo") AccountRepo repo,
                                  @Qualifier("mapper") Mapper mapper,
                                  @Qualifier("jwt-token-provider") JwtTokenProvider jwtTokenProvider) {
        this.repo = repo;
        this.mapper = mapper;
        this.jwtTokenProvider = jwtTokenProvider;
    }

    public String generateJwt(AccountAuthDto account) {
        AccountEntity accountEntity = mapper.toEntity(account);
        return jwtTokenProvider.generateToken(accountEntity);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<AccountEntity> userOptional = repo.findById(username);
        AccountEntity result = userOptional.orElseThrow(() -> new UsernameNotFoundException("Wrong credentials"));

        result.setUpdateTime(LocalDateTime.now());
        return repo.saveAndFlush(result);
    }

    @Override
    public void registerUser(AccountAuthDto user) {
        AccountEntity accountEntity = mapper.toEntity(user);

        accountEntity.setCreateTime(LocalDateTime.now());
        accountEntity.setUpdateTime(LocalDateTime.now());
        accountEntity.setPassword(passwordEncoder.encode(user.getPassword()));

        repo.saveAndFlush(accountEntity);
    }

    @Override
    public AccountDto getFullByEmail(String email) {
        return null;
    }

    @Override
    public AccountShortDto getShortByEmail(String email) {
        AccountEntity account = repo.findById(email).orElseThrow(() -> new NotFoundException("Wrong credentials"));
        return mapper.toDto(account);
    }

    @Override
    public AccountAuthDto getAuthByEmail(String email) {
        AccountEntity account = repo.findById(email).orElseThrow(() -> new NotFoundException("Wrong credentials"));
        return mapper.toDto(account.getEmail(), account.getPassword());
    }

    @Override
    public boolean validateCredentials(AccountAuthDto user) {
        if (user == null
            || user.getEmail() == null
            || user.getEmail().isBlank()
            || user.getPassword() == null
            || user.getPassword().isBlank()
        ) {
            return false;
        }

        Optional<AccountEntity> userOptional = repo.findById(user.getEmail());
        return userOptional
            .map(accountEntity -> passwordEncoder.matches(user.getPassword(), accountEntity.getPassword()))
            .orElse(false);
    }

    @Override
    public boolean validateNewEmail(String email) {
        return email != null && repo.findById(email).isEmpty();
    }

    @Override
    public boolean validateNewPassword(String password) {
        return password != null
            && password.matches(String.format("^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[%s])(?=\\S+$).{8,20}$", passSpecialCharacters));
    }
}
