package ro.contorulmeu.javaserver.service;

import ro.contorulmeu.javaserver.dto.MeterDto;
import ro.contorulmeu.javaserver.dto.MeterInfoDto;
import ro.contorulmeu.javaserver.dto.MeterShortDto;
import ro.contorulmeu.javaserver.dto.MeterSnapshotDto;
import ro.contorulmeu.javaserver.dto.MeterValueDto;

import java.util.List;

public interface MeterService {

    MeterDto getById(Long id);

    List<MeterSnapshotDto> getAll(Integer page, Integer size);

    MeterShortDto create(MeterShortDto meter);

    MeterShortDto update(Long meterId, MeterShortDto meter);

    void delete(Long id);

    List<MeterValueDto> refresh(Long meterId, MeterValueDto meterValue);

    List<MeterInfoDto> setInfo(Long meterId, MeterInfoDto meterInfo);

    List<MeterInfoDto> removeInfo(Long meterId);

    List<MeterValueDto> removeValue(Long id);

    List<MeterInfoDto> editMeterInfo(Long meterInfoId, MeterInfoDto info);
}
