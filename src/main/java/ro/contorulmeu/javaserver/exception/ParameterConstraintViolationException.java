package ro.contorulmeu.javaserver.exception;

public class ParameterConstraintViolationException extends RuntimeException{
  public ParameterConstraintViolationException() {
    super();
  }

  public ParameterConstraintViolationException(String message) {
    super(message);
  }

  public ParameterConstraintViolationException(String message, Throwable cause) {
    super(message, cause);
  }

  public ParameterConstraintViolationException(Throwable cause) {
    super(cause);
  }
}
