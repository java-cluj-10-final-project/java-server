package ro.contorulmeu.javaserver.exception;

public class OperationFailedException extends RuntimeException {
  public OperationFailedException() {
    super();
  }

  public OperationFailedException(String message) {
    super(message);
  }

  public OperationFailedException(String message, Throwable cause) {
    super(message, cause);
  }

  public OperationFailedException(Throwable cause) {
    super(cause);
  }

  protected OperationFailedException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }
}
