package ro.contorulmeu.javaserver.security.utils;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import ro.contorulmeu.javaserver.model.AccountEntity;
import ro.contorulmeu.javaserver.model.RoleEntity;

import javax.servlet.http.HttpServletRequest;
import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import static ro.contorulmeu.javaserver.constants.SecurityConstants.AUTHORITIES;
import static ro.contorulmeu.javaserver.constants.SecurityConstants.EXPIRATION_TIME;
import static ro.contorulmeu.javaserver.constants.SecurityConstants.GET_ARRAYS_ADMINISTRATION;
import static ro.contorulmeu.javaserver.constants.SecurityConstants.ISSUER;
import static ro.contorulmeu.javaserver.constants.SecurityConstants.TOKEN_CANNOT_BE_VERIFIED;

@Component("jwt-token-provider")
public class JwtTokenProvider {

  @Value("${jwt.secret}")
  private String secret;

  public String generateToken(AccountEntity user) {
    String[] claims = getClaimsFromUser(user);
    return JWT.create()
        .withIssuer(ISSUER) // who creates the token
        .withAudience(GET_ARRAYS_ADMINISTRATION) // for whom the token is created
        .withIssuedAt(new Date()) // when the token is created
        .withSubject(user.getEmail()) // who is the subject
        .withArrayClaim(AUTHORITIES, claims) // what are the claims of the subject
        .withExpiresAt(new Date(Instant.now().toEpochMilli() + EXPIRATION_TIME * 1000))
        .sign(Algorithm.HMAC512(secret.getBytes()));
  }

  public List<GrantedAuthority> getAuthorities(String token) {
    return getClaimsFromToken(token)
        .parallelStream()
        .map(s -> {
          RoleEntity roleEntity = new RoleEntity();
          roleEntity.setRole(s);
          return roleEntity;
        })
        .collect(Collectors.toList());
  }

  public Authentication getAuthentication(String email, List<GrantedAuthority> authorities, HttpServletRequest request) {
    UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(email, null, authorities);
    usernamePasswordAuthenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
    return usernamePasswordAuthenticationToken;
  }

  public Boolean isTokenValid(String email, String token) {
    return StringUtils.isNotEmpty(email)
        && !isTokenExpired(getJwtVerifier(), token);
  }

  public String getSubject(String token) {
    return getJwtVerifier()
        .verify(token)
        .getSubject();
  }

  private boolean isTokenExpired(JWTVerifier verifier, String token) {
    return verifier
        .verify(token)
        .getExpiresAt()
        .before(new Date());
  }

  private List<String> getClaimsFromToken(String token) {
    JWTVerifier verifier = getJwtVerifier();
    return verifier
        .verify(token)
        .getClaim(AUTHORITIES)
        .asList(String.class);
  }

  private JWTVerifier getJwtVerifier() {
    try {
      Algorithm algorithm = Algorithm.HMAC512(secret);
      return JWT.require(algorithm)
          .withIssuer(ISSUER)
          .build();
    } catch (JWTVerificationException e) {
      throw new JWTVerificationException(TOKEN_CANNOT_BE_VERIFIED);
    }
  }

  private String[] getClaimsFromUser(AccountEntity user) {
    return user
        .getAuthorities()
        .parallelStream()
        .map(GrantedAuthority::getAuthority)
        .collect(Collectors.toList())
        .toArray(new String[0]);
  }
}
