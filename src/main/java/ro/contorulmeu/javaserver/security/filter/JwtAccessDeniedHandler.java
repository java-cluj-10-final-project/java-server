package ro.contorulmeu.javaserver.security.filter;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.stereotype.Component;
import ro.contorulmeu.javaserver.dto.ErrorResponse;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;

import static org.springframework.http.HttpStatus.UNAUTHORIZED;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static ro.contorulmeu.javaserver.constants.SecurityConstants.ACCESS_DENIED_MESSAGE;

@Component("jwtAccessDeniedHandler")
public class JwtAccessDeniedHandler implements AccessDeniedHandler {
  private static final Log logger = LogFactory.getLog(JwtAccessDeniedHandler.class);
  private final ObjectMapper mapper = new ObjectMapper();

  @Override
  public void handle(
      HttpServletRequest request,
      HttpServletResponse response,
      AccessDeniedException exception)
      throws IOException {

    if (logger.isDebugEnabled()) {
      logger.debug("Un-authorized operation requested. Rejecting access");
    }

    ErrorResponse errorResponse = new ErrorResponse(UNAUTHORIZED, ACCESS_DENIED_MESSAGE);

    response.setContentType(APPLICATION_JSON_VALUE);
    response.setStatus(UNAUTHORIZED.value());

    OutputStream outputStream = response.getOutputStream();
    mapper.writeValue(outputStream, errorResponse);
    outputStream.flush();
  }
}
