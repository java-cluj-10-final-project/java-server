package ro.contorulmeu.javaserver.security.filter;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.Http403ForbiddenEntryPoint;
import org.springframework.stereotype.Component;
import ro.contorulmeu.javaserver.dto.ErrorResponse;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;

import static org.springframework.http.HttpStatus.FORBIDDEN;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static ro.contorulmeu.javaserver.constants.SecurityConstants.FORBIDDEN_MESSAGE;

@Component("jwtAuthenticationEntryPoint")
public class JwtAuthenticationEntryPoint extends Http403ForbiddenEntryPoint {
  private static final Log logger = LogFactory.getLog(JwtAuthenticationEntryPoint.class);
  private final ObjectMapper mapper = new ObjectMapper();

  @Override
  public void commence(
      HttpServletRequest request,
      HttpServletResponse response,
      AuthenticationException arg2)
      throws IOException {

    if (logger.isDebugEnabled()) {
      logger.debug("Pre-authenticated entry point called. Rejecting access");
    }

    ErrorResponse errorResponse = new ErrorResponse(FORBIDDEN, FORBIDDEN_MESSAGE);

    response.setContentType(APPLICATION_JSON_VALUE);
    response.setStatus(FORBIDDEN.value());

    OutputStream outputStream = response.getOutputStream();
    mapper.writeValue(outputStream, errorResponse);
    outputStream.flush();
  }
}
