package ro.contorulmeu.javaserver.security.filter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
import ro.contorulmeu.javaserver.constants.SecurityConstants;
import ro.contorulmeu.javaserver.security.utils.JwtTokenProvider;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

import static ro.contorulmeu.javaserver.constants.SecurityConstants.TOKEN_PREFIX;

@Component("jwtAuthorizationFilter")
public class JwtAuthorizationFilter extends OncePerRequestFilter {

  private final JwtTokenProvider jwtTokenProvider;

  @Autowired
  public JwtAuthorizationFilter(@Qualifier("jwt-token-provider") JwtTokenProvider jwtTokenProvider) {
    this.jwtTokenProvider = jwtTokenProvider;
  }

  @Override
  protected void doFilterInternal(
      HttpServletRequest request,
      HttpServletResponse response,
      FilterChain filterChain)
      throws ServletException, IOException {

    if (request.getMethod().equalsIgnoreCase(SecurityConstants.OPTIONS_HTTP_METHOD)) {
      // if the client wants to see the options we just let it through
      response.setStatus(HttpStatus.OK.value());
    } else {
      // start the authentication logic

      String authorizationHeader = request.getHeader(HttpHeaders.AUTHORIZATION);

      if (authorizationHeader == null || !authorizationHeader.startsWith(TOKEN_PREFIX)) {
        // if the authorization header is missing or it does not start with the prefix we defined in SecurityConstants.TOKEN_PREFIX
        // then we just let it fail the authentication

        filterChain.doFilter(request, response);

      } else {
        // we got a valid authorization information format
        String token = authorizationHeader.substring(TOKEN_PREFIX.length());
        String email = jwtTokenProvider.getSubject(token);

        if (jwtTokenProvider.isTokenValid(email, token)
            && SecurityContextHolder.getContext().getAuthentication() == null) {
          // if the token is valid and the user is not logged in already
          List<GrantedAuthority> authorities = jwtTokenProvider.getAuthorities(token);
          Authentication authentication = jwtTokenProvider.getAuthentication(email, authorities, request);
          SecurityContextHolder.getContext().setAuthentication(authentication);

        } else {
          // delete any current setup of the security context
          SecurityContextHolder.clearContext();
        }

        // after authenticating the user, we let spring go to the next filter
        filterChain.doFilter(request, response);
      }
    }
  }
}
